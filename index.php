<html lang="en" style="min-height: 625px;"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style>
.icheckbox_square-blue,
.iradio_square-blue {
    display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 22px;
    height: 22px;
    background: url(/minischool/images/blue.png) no-repeat;
    border: none;
    cursor: pointer;
}
.icheckbox_square-blue {
    background-position: 0 0;
}
    .icheckbox_square-blue.hover {
        background-position: -24px 0;
    }
    .icheckbox_square-blue.checked {
        background-position: -48px 0;
    }
    .icheckbox_square-blue.disabled {
        background-position: -72px 0;
        cursor: default;
    }
    .icheckbox_square-blue.checked.disabled {
        background-position: -96px 0;
    }

.iradio_square-blue {
    background-position: -120px 0;
}
    .iradio_square-blue.hover {
        background-position: -144px 0;
    }
    .iradio_square-blue.checked {
        background-position: -168px 0;
    }
    .iradio_square-blue.disabled {
        background-position: -192px 0;
        cursor: default;
    }
    .iradio_square-blue.checked.disabled {
        background-position: -216px 0;
    }

/* Retina support */
@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
       only screen and (-moz-min-device-pixel-ratio: 1.5),
       only screen and (-o-min-device-pixel-ratio: 3/2),
       only screen and (min-device-pixel-ratio: 1.5) {
    .icheckbox_square-blue,
    .iradio_square-blue {
        background-image: url(/minischool/images/blue@2x.png);
        -webkit-background-size: 240px 24px;
        background-size: 240px 24px;
    }
}
</style>



    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="Keywords" content="edusec,edusec4,rudra softech,open source development in ahmedabad, college management software,college management system,education management software,school management system,school management software">
	<meta name="Description" content="Software development company for open source technology in Ahmedabad. Provide Enterprise solution and quality services.">
	<meta property="og:locale" content="en_US">
	<meta property="og:title" content="Rudra Softech - Provide Enterprise Solution | Development on open source technology | College Management Software | School Management Software">
	<meta property="og:description" content="Core functions like admissions, library management, transport management, students attendance in short entire range of university functions can be well performed by EduSec">
	<meta property="og:image" content="http://www.rudrasoftech.com/rudra.png">

	<link rel="shortcut icon" href="http://localhost:3000/minischool/images/rudrasoftech_favicon.png" type="image/x-icon">
		<!-- Render this(ar-layout-css) file for supporting Arabic Language -->
		
    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token" content="MFIzbS5JVWlyanU9XgIzXAE.AyAZPT8rQQhwG20CGTlhJXomaBkmDw==">
    <title>EduSec | Login</title>

    <style>.file-input-wrapper { overflow: hidden; position: relative; cursor: pointer; z-index: 1; }.file-input-wrapper input[type=file], .file-input-wrapper input[type=file]:focus, .file-input-wrapper input[type=file]:hover { position: absolute; top: 0; left: 0; cursor: pointer; opacity: 0; filter: alpha(opacity=0); z-index: 99; outline: 0; }.file-input-name { margin-left: 8px; }</style><link href="./EduSec _ Login_files/bootstrap.css" rel="stylesheet">
<link href="./EduSec _ Login_files/AdminLTE.css" rel="stylesheet">
<link href="./EduSec _ Login_files/font-awesome.min.css" rel="stylesheet">
<link href="./EduSec _ Login_files/ionicons.min.css" rel="stylesheet">
<link href="./EduSec _ Login_files/bootstrap-multiselect.css" rel="stylesheet">
<link href="./EduSec _ Login_files/EdusecCustome.css" rel="stylesheet">
<link href="./EduSec _ Login_files/edusec-login.css" rel="stylesheet">
<script src="./EduSec _ Login_files/jquery.js"></script>
<script src="./EduSec _ Login_files/yii.js"></script>
<script src="./EduSec _ Login_files/bootstrap.js"></script>
<script src="./EduSec _ Login_files/app.js"></script>
<script src="./EduSec _ Login_files/jquery.slimscroll.min.js"></script>
<script src="./EduSec _ Login_files/bootstrap-multiselect.js"></script>
<script src="./EduSec _ Login_files/custom-delete-confirm.js"></script>
<script src="./EduSec _ Login_files/bootbox.js"></script>
<script src="./EduSec _ Login_files/bootstrap.file-input.js"></script>
<script src="./EduSec _ Login_files/bootstrapx-clickover.js"></script>
<script src="./EduSec _ Login_files/icheck.min.js"></script><style>@font-face{font-family:uc-nexus-iconfont;src:url(chrome-extension://pogijhnlcfmcppgimcaccdkmbedjkmhi/res/font_9qmmi8b8jsxxbt9.woff) format('woff'),url(chrome-extension://pogijhnlcfmcppgimcaccdkmbedjkmhi/res/font_9qmmi8b8jsxxbt9.ttf) format('truetype')}</style><style>@font-face{font-family:uc-nexus-iconfont;src:url(chrome-extension://pogijhnlcfmcppgimcaccdkmbedjkmhi/res/font_9qmmi8b8jsxxbt9.woff) format('woff'),url(chrome-extension://pogijhnlcfmcppgimcaccdkmbedjkmhi/res/font_9qmmi8b8jsxxbt9.ttf) format('truetype')}</style></head>
<body class="login-page  pace-done" style="min-height: 625px;"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>

    <div class="login-box">
      <div class="login-logo">
		&nbsp;
      </div><!-- /.login-logo -->
      <div class="login-box-body login-header">
		<h1><img src="./EduSec _ Login_files/product.png" width="150px;" alt=""></h1>
      </div>
      <div class="login-box-body">
  	
        <p class="login-box-msg">Please fill out the following fields to login</p>
	<form id="login-form" action="http://localhost:3000/minischool/index.php?r=site%2Flogin" method="post">
<input type="hidden" name="_csrf" value="MFIzbS5JVWlyanU9XgIzXAE.AyAZPT8rQQhwG20CGTlhJXomaBkmDw==">          <div class="form-group has-feedback">
	     <div class="form-group field-loginform-username required has-success">

<input type="text" id="loginform-username" class="form-control" name="LoginForm[username]" placeholder="Username">

<div class="help-block"></div>
</div>            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
	    <div class="form-group field-loginform-password required has-success">

<input type="password" id="loginform-password" class="form-control" name="LoginForm[password]" placeholder="Password">

<div class="help-block"></div>
</div>            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-7">    
              <div class="checkbox icheck">
		<div class="form-group field-loginform-rememberme">

<input type="hidden" name="LoginForm[rememberMe]" value="0"><label><div class="icheckbox_square-blue checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" id="loginform-rememberme" name="LoginForm[rememberMe]" value="1" checked="" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> Remember Me</label>

<div class="help-block"></div>
</div>              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-5">
	      <button type="submit" class="btn btn-primary btn-block btn-flat" name="login-button">Login</button>            </div><!-- /.col -->
          </div>
       </form>
        <!--a href="#">I forgot my password</a--><br>
      </div><!-- /.login-box-body -->
          </div><!-- /.login-box -->

 








<div class="login-footer"> <strong>Copyright © 2017 <a href="http://www.facebook.com/dee.promzy/">I.T promzy.org</a>.</strong> All rights reserved.</div>
<script src="./EduSec _ Login_files/yii.validation.js"></script>
<script src="./EduSec _ Login_files/yii.activeForm.js"></script>
<script type="text/javascript">jQuery(document).ready(function () {
jQuery('#login-form').yiiActiveForm([{"id":"loginform-username","name":"username","container":".field-loginform-username","input":"#loginform-username","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Username cannot be blank."});}},{"id":"loginform-password","name":"password","container":".field-loginform-password","input":"#loginform-password","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Password cannot be blank."});}},{"id":"loginform-rememberme","name":"rememberMe","container":".field-loginform-rememberme","input":"#loginform-rememberme","validate":function (attribute, value, messages, deferred, $form) {yii.validation.boolean(value, messages, {"trueValue":"1","falseValue":"0","message":"Remember Me must be either \"1\" or \"0\".","skipOnEmpty":1});}}], []);
});</script>

<script>
	$(function () {
	$('input').iCheck({
	  checkboxClass: 'icheckbox_square-blue',
	  radioClass: 'iradio_square-blue',
	  increaseArea: '20%' // optional
	});
	});
</script>
</body></html>